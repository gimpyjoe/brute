/**
 * \file modul1.h
 * \brief Plik nagłówkowy modułu logicznego \a modul1.
 *
 * Moduł \a zawiera funkcje generujące oraz sprawdzające hasło oraz struktórę przechowującą dane.
 *
 * \see modul1.cpp
 */

#ifndef MODUL1_H
#define MODUL1_H
#include <iostream>
#include <string>
#include <cmath>
#include "modul2.h"

using namespace std;

/** \brief Element listy dynamicznej.
*
* Definicja elementu jednokierunkowej dynamicznej listy.
*/
struct lista
{
    ///wartość polawskazującego na odpowiednią literę w słowniku
    int data;
    ///wskaźnik na kolejny element listy
    lista*next;
};


/**
 * \brief Procedura zmieniająca listę na podstawie której tworzone są słowa.
 * Na zasadzie dodawania 1 do liczby w stystemie liczbowym stworzonym na podstawie długości słownika możliwych znaków.
 * \param dl długość słownika znaków
 * \param  *glowa lista do tworzenia słów.
 */

void zmien(int dl,lista* glowa);


/**
 * \brief Procedura tłumacząca listę liczb na wyrazy zgodnie ze słownikiem możliwych znaków.
 * \param litery słownik możliwych znaków.
 * \param glowa lista do tworzenia słów.
 */


void podstaw(string &odpwoiedz,string litery,lista* glowa);


/**
 * @brief Główna procedura logiczna.
 * Procedura porównująca hasło z bierzącą odpowiedzią,
 * oraz zwiększająca długość listy jeśli wszystkie hasła o bierzącej długości zostały sprawdzone.
 * \param glowa lista do tworzenia słów.
 * \param odpowiedz bierząca odpowiedź.
 * \param hasło podane przez użytkownika hasło.
 * \param litery słownik możliwych znaków.
 * \param e wartość pokazująca czy występuje fragment.
 * \param czesc podany przez użytkownika fragment hasła.
 * \param litery słownik możliwych znaków.
 */


void brute(lista* glowa,string &odpowiedz,string haslo,string litery,bool e,string czesc);


/**
 * \brief Funkcja wstawiająca fragment pomiedzy każde dwie litery wygenerowanego słowa.
 * \param odpowiedz bierząca odpowiedź.
 * \param hasło podane przez użytkownika hasło.
 * \param czesc podany przez użytkownika fragment hasła.
 * \return informacja czy odnaleziono hasło.
 */

bool fragment(string &odpowiedz,string haslo,string czesc);


#endif // MODUL1_H
