/**
 * \file main.cpp
 * \brief Plik z funkcją main()
 */


/**
* \mainpage
* \par Łamanie szyfrów bruteforce.
* \author Paweł Witek
* \date 2014.06.06
* \version 1.0
*/



#include <iostream>
#include <string>
#include <ctime>
#include <cmath>
#include "modul1.h"
#include "modul2.h"

using namespace std;


/**
 * \file main.cpp
 * \brief funkcja main tworząca zmienne, uruchamiająca funkcje i usuwająca zmienne.
 */


int main()
{

    bool a=0,b=0,c=0,d=0,e=0;

    string haslo,czesc,litery;

    string odpowiedz;

    wczytaj(a,b,c,d,e,litery,haslo, czesc);
    int start, end;
    start = time(NULL);
    lista*glowa;
    glowa = new lista;
    glowa->data=0;
    glowa->next=NULL;



    brute(glowa,odpowiedz,haslo,litery,e,czesc);
    cout<<"szukane haslo:"<<odpowiedz<<endl;




    end = time(NULL);
    cout<<"czas:"<<end - start<<"s"<<endl;


    lista*a1,*a2;
    a1=glowa;
    a2=glowa;

    while(a1->next!=NULL)
    {

        a1=a1->next;
        delete a2;
        a2=a1;
    }
    delete a2;



}
