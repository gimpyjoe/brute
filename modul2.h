/**
 * \file modul2.h
 * \brief Plik nagłówkowy modułu interfejsu \a modul2.
 *
 * Moduł \a modul2 zawiera funkcje niezbędne
 * do komunikacji z użytkownikiem poprzez konsolę
 * tekstową.
 *
 * \see modul2.cpp
 */


#ifndef MODUL2_H
#define MODUL2_H
#include <iostream>
#include <string>
using namespace std;


/**
 * @brief Główna procedura interfejsu.
 * \param a cyfry
 * \param b małe litery
 * \param c wielkie litery
 * \param d znaki specjalne
 * \param e fragment czy występuje
 * \param litery słownik możliwych znaków
 * \param czesc wartość fragmentu hasła
 * Procedura realizująca komunikacją z użytkownikiem
 * poprzez interfejs konsolowy.
 */

void wczytaj(bool &a,bool &b,bool &c,bool &d,bool &e,string &litery,string &haslo,string &czesc);

/**
 * \Param x aktualnie pobierana wartość.
 * \brief Procedura pobiera wartość do zmiennej bool dopóki użytkownik nie poda właściwych danych.
 */

void sprawdz(bool &x);

/**
 * \param a cyfry
 * \param b małe litery
 * \param c wielkie litery
 * \param d znaki specjalne
 * \param litery słownik możliwych znaków
 * \brief Procedura tworzy słownik możliwych znaków na podstawie danych od użytkownika.
 */

void pokolei(bool a,bool b,bool c,bool d,string &litery);

#endif // MODUL2_H
